/*
 * @class VectorTile
 * @inherits TileLayer
 * @aka L.VectorTile
 *
 * Class for handling GeoJSON Vector Tiles. Tiles must be prepared by backend. Tiles must have a id property to distinct objects on tile edges.
 * Tiles was loading asynchronously (promises).
 *
 * @section Usage
 * @example
 *
 * You need to pass url with tiles and function to parse data. GeoJSON 'onEachFeature' behavior is also supported
 *
 * ```js
 * //if data is an GeoJSON object
 * parser = function (data) {
 * return [data];
 * };
 * var geojsonURL = '/tile/{z}/{x}/{y}/';
 * var layer = L.vectorTile({url: geojsonURL, minZoom: 1, maxZoom: 16, parser: parser, onEachFeature: onEach});
 * map.addLayer(layer); //add layer to map
 * ```
 */

L.VectorTile = {};
L.VectorTile = L.TileLayer.extend({
    // @section
    // @aka VectorTile options
    options: {
        // @option parser: function
        // pass a function to prepare data. Must return map of GeoJSON objects (see example)
        parser: undefined,
    },

    initialize: function (options) {
        //call super
        L.TileLayer.prototype.initialize.call(this, options);
        L.setOptions(this, options);
        this.layerGroup = new L.MultiLayerGroup();
    },
    //set style for element by id
    setStyle: function (style, id) {
        const layer = this.layerGroup.getLayer(id);
        if (layer)
            for (var i in layer) {
                layer[i].setStyle(style);
            }
    },
    //
    createTile: function (coords, done) {
        const vectorTilePromise = this._getVectorTilePromise(coords);
        const _this = this;
        return vectorTilePromise.then(function renderTile(vectorTile) {
            for (const layer of vectorTile) {
                if (!layer)
                    continue;
                // Transform the geojson into a new Layer
                try {
                    const incomingLayer = new L.GeoJSON(layer, {onEachFeature: _this.options.onEachFeature});
                    const key = _this._tileCoordsToKey(coords);
                    const tile = _this._tiles[key];
                    if (tile && !tile.canceled) {
                        tile.el = incomingLayer;
                        _this.layerGroup.addLayer(incomingLayer);
                    }
                }
                catch (e) {
                    console.log(e);
                    return;
                }
            }
        });
    },
    _addTile: function (coords, container) {
        const key = this._tileCoordsToKey(coords);

        this.createTile(this._wrapCoords(coords), L.bind(this._tileReady, this, coords));
        // // save tile in cache
        this._tiles[key] = {
            coords: coords,
            current: true,
            canceled: false
        };

    },
    _getVectorTilePromise: function (coords) {
        const url = L.Util.template(this._url.url, L.extend(coords, this.options));
        const p = new Promise(function (resolve, reject) {
            const request = new XMLHttpRequest();
            request.open('GET', url);
            request.responseType = 'json';
            request.onload = function () {
                if (request.status === 200) {
                    resolve(request.response);
                } else {
                    // If it fails, reject the promise with a error message
                    reject(Error(request.statusText));
                }
            };
            request.onerror = function () {
                reject(Error('There was a network error.'));
            };
            request.send();
        });
        return p.then(this.options.parser);
    },
    _removeTile: function (key) {
        const tile = this._tiles[key];
        if (!tile) {
            return;
        }
        if (tile.el)
            this.layerGroup.removeLayer(tile.el);
        delete this._tiles[key];
    },
    _abortLoading: function () {
        for (const i in this._tiles) {
            if (this._tiles[i].coords.z !== this._tileZoom) {
                this._tiles[i].canceled = true;
            }
        }
    },
    onAdd: function (map) {
        this._map = map;
        L.TileLayer.prototype.onAdd.call(this, map);
        map.addLayer(this.layerGroup);
    },
    onRemove: function (map) {
        this._map = null;
        L.TileLayer.prototype.onRemove.call(this, map);
        map.removeLayer(this.layerGroup);
    },
});

L.vectorTile = {};
L.vectorTile = function (options) {
    return new L.VectorTile(options);
};
/*
 * @class MultiLayerGroup
 * @inherits FeatureGroup
 * @aka L.MultiLayerGroup
 *
 * Class for internal usage for VectorTile layer. Used for displaying same objects from different tiles only once.
 *
 */
L.MultiLayerGroup = L.FeatureGroup.extend({
    // @method addLayer(layer: Layer): this
    // Adds the given layer to the group.
    addLayer: function (layer) {
        if (typeof layer.getLayers === "function") {
            const layers = layer.getLayers();
            for (const l of layers)
                this.addLayer(l);
            return;
        }
        const id = this.getLayerId(layer);
        const genericId = L.LayerGroup.prototype.getLayerId.call(this, layer);
        const previous = this._layers[id];
        if (!previous) {
            this._layers[id] = {};
            this._layers[id][genericId] = layer;
        }
        else {
            this._layers[id][genericId] = layer;
            return this;
        }
        if (this._map) {
            this._map.addLayer(layer);
        }
        layer.addEventParent(this);
        // @event layeradd: LayerEvent
        // Fired when a layer is added to this `FeatureGroup`
        return this.fire('layeradd', {layer: layer});
    },

    getLayer: function (id) {
        return this._layers[id];
    },

    removeLayer: function (layer) {
        if (typeof layer.getLayers === "function") {
            const layers = layer.getLayers();
            for (const l of layers)
                this.removeLayer(l);
            return;
        }
        const id = this.getLayerId(layer);
        const genericId = L.LayerGroup.prototype.getLayerId.call(this, layer);
        const layerAgg = this._layers[id];
        if (!layerAgg)
            return this;
        if (Object.keys(layerAgg).length > 1 && layerAgg[genericId]) {
            if (this._map && this._map.hasLayer(layerAgg[genericId])) {
                this._map.removeLayer(layerAgg[genericId]);
                delete layerAgg[genericId];
                this._map.addLayer(layerAgg[Object.keys(layerAgg)[0]]);
            }
            else
                delete layerAgg[genericId];
            return this;
        }
        layer.removeEventParent(this);
        if (this._map) {
            this._map.removeLayer(layerAgg[genericId]);
        }
        delete this._layers[id];


        // @event layerremove: LayerEvent
        // Fired when a layer is removed from this `FeatureGroup`
        return this.fire('layerremove', {layer: layer});
    },
    // @method getLayerId(layer: Layer): Number
    // Returns the internal ID for a layer
    getLayerId: function (layer) {
        if (layer.feature)
            return layer.feature.properties.id;
        return L.stamp(layer);
    },
    // @method getBounds(): LatLngBounds
    // Returns the LatLngBounds of the Feature Group (created from bounds and coordinates of its children).
    getBounds: function () {
        const bounds = new L.LatLngBounds();

        for (const id in this._layers) {
            let layer = this._layers[id];
            layer = layer[Object.keys(layer)[0]]; //get first element, they are equal
            bounds.extend(layer.getBounds ? layer.getBounds() : layer.getLatLng());
        }
        return bounds;
    }
});