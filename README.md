Class for handling GeoJSON Vector Tiles. Tiles must be prepared by backend. Tiles must have a id property to distinct objects on tile edges.
 Tiles was loading asynchronously (promises).

 Usage

 You need to pass url with tiles and function to parse data. GeoJSON 'onEachFeature' behavior is also supported

```js
 //if data is an GeoJSON object
 parser = function (data) {
 return [data];
 };
 var geojsonURL = '/tile/{z}/{x}/{y}/';
 var layer = L.vectorTile({url: geojsonURL, minZoom: 1, maxZoom: 16, parser: parser, onEachFeature: onEach});
 map.addLayer(layer); //add layer to map
```